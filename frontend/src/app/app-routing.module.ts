import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SinglePageComponent} from "./single-page/single-page.component";

const routes: Routes = [
  {path: '', component: SinglePageComponent},
  {path: 'about', component: SinglePageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
