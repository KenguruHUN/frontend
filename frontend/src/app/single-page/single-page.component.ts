import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'app-single-page',
  templateUrl: './single-page.component.html',
  styleUrls: ['./single-page.component.css']
})
export class SinglePageComponent implements OnInit {
  private page: { }[] = [];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    console.log(isNullOrUndefined(this.route.snapshot.url[0].path));
    console.log(this.route.snapshot.url[0].path);
  }

}
