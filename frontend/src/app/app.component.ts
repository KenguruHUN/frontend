import { Component } from '@angular/core';
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  titleText = 'frontend';
  viewportText = 'width=device-width, initial-scale=1';
  descriptionText = 'Handcrafted django blog and portfolio site with angular frontend.';
  keywordsText = "django, portfolio, blog";
  authorText = 'Greg Csokas';

  constructor(private title: Title, private meta: Meta) {
    this.title.setTitle(
      this.titleText
    );

    this.meta.updateTag(
      { name: 'viewport', content: this.viewportText}
      );

    this.meta.updateTag(
      { name: 'description', content: this.descriptionText}
    );

    this.meta.updateTag(
      { name: 'keywords', content: this.keywordsText}
    );

    this.meta.updateTag(
      { name: 'author', content: this.authorText}
    );
  }
}
